const { test } = require('ava');
const agent = require('supertest');
const createApp = require('../src/app');
const { jwtHelper } = require('./helpers');

const app = agent(createApp());
const authHeader = `Bearer ${jwtHelper({ id: 5 })}`;

test('Get user list', async (t) => {
  const { body, status } = await app.get('/user/list')
    .set('Authorization', authHeader);

  t.is(status, 200);
  t.truthy(Array.isArray(body));
});

test('Get user by id', async (t) => {
  const res = await app.get(`/user/1`)
    .set('Authorization', authHeader);

  t.is(res.status, 200);
});

test('Get user by invalid id should be 404', async (t) => {
  const res = await app.get('/user/666')
    .set('Authorization', authHeader);

  t.is(res.status, 404);
});
