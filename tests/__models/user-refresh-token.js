const { find, reject } = require('lodash');

let { userRefreshTokens } = require('../__data');

class UserRefreshToken {
  static async create(data) {
    userRefreshTokens.push(data);
    return userRefreshTokens;
  }

  static async findOne(query) {
    return find(userRefreshTokens, query.where);
  }

  static async remove(query) {
    userRefreshTokens = reject(userRefreshTokens, query);
    return userRefreshTokens;
  }
}

module.exports = UserRefreshToken;
