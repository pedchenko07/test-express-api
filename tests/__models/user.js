const { find } = require('lodash');

const { users } = require('../__data');

class User {
  static async create(user) {
    const data = { ...user, id: 3 };
    users.push(data);
    return data;
  }

  static async findAll() {
    return users;
  }

  static async findById(userId) {
    return users.find(({ id }) => id === userId);
  }

  static async findOne(query) {
    return find(users, query);
  }

}

module.exports = User;
