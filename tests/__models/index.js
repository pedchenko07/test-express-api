const User = require('./user');
const UserRefreshToken = require('./user-refresh-token');

module.exports = {
  UserRefreshToken,
  User,
};
