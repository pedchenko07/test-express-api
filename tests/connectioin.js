const { test } = require('ava');
const { connection } = require('../src/db/index');

test('DB connection', async (t) => {
  await t.notThrows(connection.authenticate());
});
