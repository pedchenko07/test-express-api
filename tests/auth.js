const { test } = require('ava');
const agent = require('supertest');

const createApp = require('../src/app');
const { jwtHelper } = require('./helpers');

const app = agent(createApp());

test('User can register', async (t) => {
  const res = await app.post('/auth/register').send({
    lastName: 'Register',
    firstName: 'Register',
    email: 'register@email.loc',
    password: 'register',
  });

  t.is(res.status, 200);
  t.truthy(typeof res.body.token === 'string');
  t.truthy(typeof res.body.refreshToken === 'string');
});

test('User get errors when send invalid params', async (t) => {
  const res = await app.post('/auth/register').send({
    lastName: '',
    firstName: '',
    email: '',
    password: '',
  });

  t.is(res.status, 400);
});

test('User can successfully login', async (t) => {
  const res = await app
    .post('/auth/login')
    .send({ login: 'test1@email.com', password: 'test1@email.com' });

  t.is(res.status, 200);
  t.truthy(typeof res.body.token === 'string');
  t.truthy(typeof res.body.refreshToken === 'string');

  const tokens = await app
    .post('/auth/refresh')
    .send({ refreshToken: res.body.refreshToken });

  t.is(tokens.status, 200);
  t.truthy(typeof tokens.body.token === 'string');
  t.truthy(typeof tokens.body.refreshToken === 'string');
});

test('User get 403 on invalid credentials', async (t) => {
  const res = await app
    .post('/auth/login')
    .send({ login: 'user', password: 'user' });

  t.is(res.status, 403);
});

test('User receives 401 on expired token', async (t) => {
  const expireHeader = `Bearer ${jwtHelper({ id: 5 }, { expiresIn: '1ms' })}`;
  const res = await app.get('/user/list').set('Authorization', expireHeader);

  t.is(res.status, 401);
});

test('User can get new access token using refresh token', async (t) => {
  const res = await app
    .post('/auth/refresh')
    .send({ refreshToken: 'REFRESH_TOKEN_1' });

  t.is(res.status, 200);
  t.truthy(typeof res.body.token === 'string');
  t.truthy(typeof res.body.refreshToken === 'string');
});

test('User get 404 send invalid refresh token', async (t) => {
  const res = await app
    .post('/auth/refresh')
    .send({ refreshToken: 'INVALID_REFRESH_TOKEN' });

  t.is(res.status, 404);
});

test('User can refresh token only once', async (t) => {
  const firstResponse = await app
    .post('/auth/refresh')
    .send({ refreshToken: 'REFRESH_TOKEN_ONCE' });

  t.is(firstResponse.status, 200);
  t.truthy(typeof firstResponse.body.token === 'string');
  t.truthy(typeof firstResponse.body.refreshToken === 'string');

  const secondResponse = await app
    .post('/auth/refresh')
    .send({ refreshToken: 'REFRESH_TOKEN_ONCE' });

  t.is(secondResponse.status, 404);
});

test('Refresh tokens become invalid on logout', async (t) => {
  const expireHeader = `Bearer ${jwtHelper({ id: 99 })}`;
  const logoutRes = await app
    .post('/auth/logout')
    .set('Authorization', expireHeader);

  t.is(logoutRes.status, 200);

  const response = await app
    .post('/auth/refresh')
    .send({ refreshToken: 'REFRESH_TOKEN_FOR_LOGOUT' });

  t.is(response.status, 404);
});
test('Multiple refresh tokens are valid', async (t) => {
  const loginResponseFirst = await app
    .post('/auth/login')
    .send({ login: 'test2@email.com', password: 'test2@email.com' });

  t.is(loginResponseFirst.status, 200);

  const loginResponseSecond = await app
    .post('/auth/login')
    .send({ login: 'test2@email.com', password: 'test2@email.com' });

  t.is(loginResponseSecond.status, 200);

  const firstResponseRefresh = await app
    .post('/auth/refresh')
    .send({ refreshToken:  loginResponseFirst.body.refreshToken});

  t.is(firstResponseRefresh.status, 200);

  const secondResponseRefresh = await app
    .post('/auth/refresh')
    .send({ refreshToken:  loginResponseSecond.body.refreshToken});

  t.is(secondResponseRefresh.status, 200);
});
