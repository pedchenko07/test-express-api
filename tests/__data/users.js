const {hashSync} = require('bcryptjs');

module.exports = [
  {
    id: 1,
    firstName: 'Test1',
    lastName: 'Test1',
    email: 'test1@email.com',
    password: hashSync('test1@email.com'),
  },
  {
    id: 2,
    firstName: 'Test2',
    lastName: 'Test2',
    email: 'test2@email.com',
    password:  hashSync('test2@email.com'),
  },
];
