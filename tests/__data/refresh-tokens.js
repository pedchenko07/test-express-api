module.exports = [
  {
    userId: 9,
    refreshToken: 'REFRESH_TOKEN_1',
  },
  {
    userId: 8,
    refreshToken: 'REFRESH_TOKEN_ONCE',
  },
  {
    userId: 99,
    refreshToken: 'REFRESH_TOKEN_FOR_LOGOUT',
  },
];
