const jwt = require('jsonwebtoken');
const { SECRET } = require('../../src/config');

module.exports = (data, options = {}) => jwt.sign(data, SECRET, options);
