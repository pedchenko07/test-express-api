const { Router } = require('express');

const { UserService } = require('../modules');

const router = new Router();

router.get('/list', async (req, res) => {
  const users = await UserService.getList();

  res.send(users);
  res.end();
});

router.get('/:id', async (req, res) => {
  const id = Number(req.params.id);
  const user = await UserService.getUserById(id);

  if (!user) {
    res.status(404);
    return res.end();
  }

  res.send(user);
  return res.end();
});

module.exports = router;
