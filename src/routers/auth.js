const { Router } = require('express');
const { compareSync } = require('bcryptjs');
const jwtMiddleware = require('express-jwt');

const { UserService, AuthService } = require('../modules');
const { userValidator } = require('../middleware');
const { SECRET } = require('../config');

const router = new Router();

router.post('/register', userValidator, async (req, res) => {
  const { body } = req;

  const user = await UserService.create(body);
  const { token, refreshToken } = AuthService.generateTokens(user.id);

  await AuthService.addRefreshToken({ refreshToken, userId: user.id });

  res.send({
    token,
    refreshToken,
  });
  res.end();
});

router.post('/login', async (req, res) => {
  const { login, password } = req.body;
  const user = await UserService.findOne({ email: login });

  if (!user || !compareSync(password, user.password)) {
    return res.status(403).end();
  }

  const { token, refreshToken } = AuthService.generateTokens(user.id);
  await AuthService.addRefreshToken({ userId: user.id, refreshToken });

  res.send({
    token,
    refreshToken,
  });

  return res.end();
});

router.post('/refresh', async (req, res) => {
  const { body } = req;
  const dbToken = await AuthService.getObjectByRefreshToken(body.refreshToken);

  if (!dbToken) {
    return res.status(404).end();
  }

  const { userId } = dbToken;
  const { token, refreshToken } = AuthService.generateTokens(userId);
  await AuthService.remove({ refreshToken: body.refreshToken });
  await AuthService.addRefreshToken({ userId, refreshToken });

  res.status(200);
  res.send({
    token,
    refreshToken,
  });
  return res.end();
});

router.post('/logout', jwtMiddleware({ secret: SECRET }), async (req, res) => {
  const { id: userId } = req.user;
  await AuthService.remove({ userId });
  res.end();
});

module.exports = router;
