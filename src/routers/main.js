const { Router } = require('express');

const router = new Router();

// eslint-disable-next-line no-unused-vars
router.get('/', (req, res, next) => {
  res.sendStatus(200);
});

module.exports = router;
