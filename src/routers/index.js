const userRouter = require('./user');
const mainRouter = require('./main');
const authRouter = require('./auth');

module.exports = {
  userRouter,
  mainRouter,
  authRouter,
};
