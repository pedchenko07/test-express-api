const Express = require('express');
const { json } = require('body-parser');
const jwtMiddleware = require('express-jwt');

const { userRouter, mainRouter, authRouter } = require('./routers');
const { PORT, SECRET } = require('./config');

function createApp() {
  const app = Express();

  app.use(json());

  app.use('/', mainRouter);
  app.use('/auth', authRouter);
  app.use(jwtMiddleware({ secret: SECRET }));
  app.use('/user', userRouter);

  app.use((err, req, res) => {
    // eslint-disable-next-line no-console
    console.error(err.stack);
    res.status(400).send(`Something broke! ${err.message}`);
  });

  return app;
}

if (!module.parent) {
  createApp().listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`server start on port: ${PORT}`);
  });
}

module.exports = createApp;
