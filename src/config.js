const rc = require('rc');

module.exports = rc('TEST_EXPRESS_API', {
  PORT: process.env.PORT || 3000,
  DB_NAME: 'test-express-api',
  DB_USERNAME: 'Roman',
  DB_PASS: '123',
  DB_HOST: '127.0.0.1',
  DB_DIALECT: 'mysql',
  PATH_MODELS: 'src/models',
  SECRET: 'VERYSECRETKEY',
});
