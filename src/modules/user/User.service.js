const { resolve } = require('path');
const { hashSync } = require('bcryptjs');
const { PATH_MODELS } = require('../../config');

// eslint-disable-next-line import/no-dynamic-require
const { User } = require(resolve(__dirname, '..', '..', '..', PATH_MODELS));

class UserService {
  static create({ password, ...args }) {
    const user = { ...args, password: hashSync(password) };
    console.log(user);
    return User.create(user);
  }

  static getList() {
    return User.findAll();
  }

  static async getUserById(id) {
    return User.findById(id);
  }

  static findOne(...args) {
    return User.findOne(...args);
  }
}

module.exports = UserService;
