const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');
const { resolve } = require('path');

const { PATH_MODELS, SECRET } = require('../../config');

// eslint-disable-next-line import/no-dynamic-require
const { UserRefreshToken } = require(resolve(
  __dirname,
  '..',
  '..',
  '..',
  PATH_MODELS,
));

class AuthService {
  static generateTokens(id) {
    return {
      refreshToken: uuid(),
      token: jwt.sign({ id }, SECRET, { expiresIn: '60000ms' }),
    };
  }

  static async addRefreshToken(data) {
    return UserRefreshToken.create(data);
  }

  static async getObjectByRefreshToken(refreshToken) {
    return UserRefreshToken.findOne({ where: { refreshToken } });
  }

  static async remove(query) {
    return UserRefreshToken.remove(query);
  }
}

module.exports = AuthService;
