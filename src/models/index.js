const User = require('./User');
const UserRefreshToken = require('./UserRefreshToken');

module.exports = {
  User,
  UserRefreshToken,
};
