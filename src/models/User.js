const Model = require('./Model');

class User extends Model {
  static create(user) {
    return this.model.create(user);
  }

  static findOne(query) {
    return this.model.findOne({ where: query });
  }
}

module.exports = User;
