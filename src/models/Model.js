const { connection } = require('../db');
const models = require('../db/models');

class Model {
  static get connection() {
    return connection;
  }

  static get models() {
    return models;
  }

  static get model() {
    return this.models[this.name];
  }

  static findOne(...args) {
    return this.model.findOne(...args);
  }

  static findById(...args) {
    return this.model.findById(...args);
  }

  static findAll(...args) {
    return this.model.findAll(...args);
  }

  static remove(...args) {
    return this.model.destroy(...args);
  }
}

module.exports = Model;
