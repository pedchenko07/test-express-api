const Model = require('./Model');

class UserRefreshToken extends Model {
  static create(data) {
    return this.model.create(data);
  }

  static remove(query) {
    return this.model.destroy({ where: query });
  }
}

module.exports = UserRefreshToken;
