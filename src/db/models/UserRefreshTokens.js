const path = require('path');
const { DataTypes } = require('sequelize');

const User = require('./User');
const connection = require('../connection');

const modelName = path.basename(__filename, '.js');

const model = connection.define(
  modelName,
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: 'id',
      },
    },
    refreshToken: DataTypes.STRING,
  },
  {
    tableName: modelName,
    timestamps: false,
  },
);

module.exports = model;
