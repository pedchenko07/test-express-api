const path = require('path');
const { DataTypes } = require('sequelize');

const connection = require('../connection');
const UserRefreshTokens = require('./UserRefreshTokens');

const modelName = path.basename(__filename, '.js');

const model = connection.define(
  modelName,
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    email: DataTypes.STRING,
    firstName: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
        len: [1, 255],
      },
    },
    lastName: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: modelName,
  },
);

model.hasMany(UserRefreshTokens, { as: 'refreshToken' });

module.exports = model;
