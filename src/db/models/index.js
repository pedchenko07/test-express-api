const User = require('./User');
const UserRefreshToken = require('./UserRefreshTokens');

module.exports = {
  User,
  UserRefreshToken,
};
