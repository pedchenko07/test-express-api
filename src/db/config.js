const {
  DB_USERNAME,
  DB_PASS,
  DB_NAME,
  DB_HOST,
  DB_DIALECT,
} = require('../config');

module.exports = {
  username: DB_USERNAME,
  password: DB_PASS,
  database: DB_NAME,
  host: DB_HOST,
  dialect: DB_DIALECT,
};
