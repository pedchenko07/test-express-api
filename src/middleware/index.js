const { userValidator } = require('./validators');

module.exports = {
  userValidator,
};
