const Joi = require('joi');

const { userSchema } = require('../../schemas');

module.exports = async ({ body }, res, next) => {
  const { error } = Joi.validate(body, userSchema);
  if (error) {
    return res
      .status(400)
      .send(error)
      .end();
  }
  return next();
};
